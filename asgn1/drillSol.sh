#Section 1
mkdir hello		#Creating hello folder
cd hello		#inside hello
mkdir five one		#Creating folder five
cd five			# inside five
mkdir six		#Creating folder six
cd six			#Inside six
touch c.txt		#Creating file c.text
mkdir seven		#Creating folder seven
cd seven		#inside seven
touch error.log		#creating file error.log
cd ~/hello		#back to hello
cd one			#inside one
touch a.text b.txt	#creating two files a and b respectively
mkdir two		#creating folder two inside one
cd two			#inside two
touch d.txt		#creating file d.txt
mkdir three		#creating folder three
cd three		# inside three
touch e.txt		#creating file e.txt
mkdir four		#creating folder four
cd four			#inside four
# ladwda lasun
touch access.log	#creating file access.log

# Section 2
rm ./hello/one/two/three/four/access.log ./hello/five/six/seven/error.log		#removing all .log

# Section 3
cd one			#inside one
nano a.txt		#writing given text in a.txt file using nano

#Section 4
rm -R five		#removing non-empty folder five

#Section 5
cd one			#inside one
mv one uno		#renaming one folder as "uno"
# Section 6
cd uno			#inside uno
mv a.txt two		#moving a.txt file from uno to two
